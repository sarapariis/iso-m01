# !/bin/bash
# @Sara París
# Febrer 2024
# Descripció: exemples while
# -----------------------------------
# 8) processar linia a linia un fitxer
fileIn=$1
num=1
while read -r line 
do
   chars=$(echo $line | wc -c)
   echo "$num: ($chars) $line " | tr 'a-z' 'A-Z'
   ((num++))
done < $fileIn
exit 0

# 7) numerar i mostrar en majuscules(stdin)
num=1
while read -r line
do
   echo "$num: $line" | tr 'a-z' 'A-Z'
   ((num++))
done
exit 0

# 6) itera linia linia fins a token (per exemple FI)
TOKEN=FI
num=1
read -r line
while [ $line != $TOKEN ] 
do
   echo "$num: $line"
   read -r line
   ((num++))
done
exit 0

# 5) numerar les linies rebudes
num=1
while read -r line
do
   echo "$num: $line"
   ((num++))
done
exit 0
#4) processar stdin linia a linia
while read -r line 
do
   echo $line
done
exit 0

# 3) iterar la llista d'args (normalment for)
while [ -n "$1"  ]
do
   echo "$1 $# $*"
   shift
done
exit 0

# 2) contador decrementa valor rebut (rep arg(no validació) fa un compte enrera)
MIN=0
num=$1
while [ $num -ge $MIN ]
do
	echo -n "$num "
	(($num--))
done
exit 0
# 1) mostrar numeror de l'1 al 10
MAX=10
num=1
while [ $num -le $MAX  ]
do
   echo -n "$num "
   ((num++))
done
exit 0

