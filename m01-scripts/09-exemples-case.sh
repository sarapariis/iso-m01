# !/bin/bash
# @Sara París
# Febrer 2024
# Exemples case
# ---------------------------
#
#dl dt dc dj dv --> laborals
#ds dg --> festius
#altre cosa

case $1 in
  "dl" | "dm" | "dc" | "dj" | "dv")
    echo "$1 es un dia laboral";;
  "ds" | "dg")
    echo "$1 es una dia festiu";;
  *)
    echo "això ($1) no es una dia";;
esac
exit 0
#
# exemple vocals
case $1 in
  [aeiou])
    echo "$1 és una vocal"
    ;;
  ![aeiou])
    echo "$1 es una altre cosa"
    ;;
  aeiou*)
    echo "$1 és una consonant"
    ;;
esac
exit 0
