# !/bin/bash
# @Sara París
# Febrer 2024
# Fer un programa que rep com a arguments noms de dies de la setmana i mostra
# quants dies eren laborables i quants festius. Si l’argument no és un dia de la
# setmana genera un error per stderr.
# Exemple: $ prog dilluns divendres dilluns dimarts kakota dissabte sunday
# -------------------------------------------------------------------
ERR_NARGS=1
ERR_ARGVL=2
# 1)validar args.
if [ $# -eq 0 ]
then
  echo "Error numero arguments incorrecte"
  echo "Usage: $0 dies"
  exit $ERR_NARGS 
fi
# 2) definir
for dia in $*
do
	case $dia in
		"dilluns"|"dimarts"|"dimecres"|"dijous"|"divendres")
			((laborables++));;
		"dissabte"|"diumenge")
			((festius++));;
		*)
		echo "el dia $dia no es un dia de la setmana" >>/dev/stderr;;		
	esac
done
echo "Total de dies laborables: $laborables"
echo "Total de dies festius: $festius"
exit 0
