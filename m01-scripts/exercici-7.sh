# !/bin/bash
# @Sara París
# Febrer 2024
# Processar línia a línia l’entrada estàndard, si la línia té més de 60 caràcters la mostra, si no no.
# -----------------------------------------------------------------------
while read -r line 
do
   chars=$(echo $line | wc -c)
   echo "($chars) $line " | tr 'a-z' 'A-Z'
   ((num 
exit 0

