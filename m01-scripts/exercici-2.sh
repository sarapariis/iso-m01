# !/bin/bash
# @Sara París
# Febrer 2024
# Mostar els arguments rebuts línia a línia, tot numerànt-los.
# ----------------------------------------------------
num=1
for args in $*
do
echo "$num: $args"
((num++))
done
exit 0
