# !/bin/bash
# @Sara París
# Febrer 2024
# prog dir
# a) rep un arg i es un directori es llista
# b) llistar numerant els elements del directori
# ---------------------------------------------
ERR_ARGS=1
ERR_NODIR=2
# si num args no es correcte plegar
if [ $# -ne 1 ]; then
  echo "ERROR: num args incorrecte"
  echo "usage: $0 dir"
  exit $ERR_ARGS
fi
dir=$1
# si no és un directori plegar
if [ ! -d $dir  ]; then
  echo "ERROR: $dir no és un directori"
  echo "usage: $0 dir"
  exit $ERR_NODIR
fi
# xixa: llistar el directori
num=1
llistat=$(ls $dir)
for elem in $llistat
do
   echo "$num: $elem"
   ((num++))
done
exit 0

