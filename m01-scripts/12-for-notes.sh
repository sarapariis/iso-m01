# !/bin/bash
# @Sara París
# Febrer 2024
# Descripció: suspès, aprovat, exel·lent
# ---------------------------------
# 1) validar args
if [ $# -eq 0 ]; then
   echo "Error: num args"
   echo "Usage: $0 nota..."
   exit 1
fi
# 2) iterar llista args
for nota in $*
do
   if ! [ $nota -ge 0 -a $nota -le 10 ]; then
      echo "Error: nota $nota no vàlida (0-10)" >> /dev/stderr      
   else 
      if [ $nota -lt 5 ]; then
	      echo "suspès"
      elif [ $nota -lt 7 ]; then
	      echo "aprovat"
      else
	      echo "notable"
      fi
fi
done
exit 0
