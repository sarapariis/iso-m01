#! /bin/bash
# @Sara París
# Febrer 2024
# Validar que té exàctament 2 args
# i mostarr-los
#
# 04-validar-arguments nom cognom
# -------------------------------
# 1)si num args no es correcte plegar
if [ $# -ne 2 ]
then
  echo "ERROR: num args incorrecte"
  echo "USAGE: $0 nom edat"  
  exit 1
fi	
# 2) xixa
echo "nom: $1"
echo "edat: $2"
exit 0

