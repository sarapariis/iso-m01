# !/bin/bash
# @Sara París
# Febrer 2024
# Fer un programa que rep com a arguments números de més (un o més) i indica per
# a cada mes rebut quants dies té el més.
# ----------------------------------------------------------------------------
ERR_NARGS=1
ERR_ARGVL=2
mes=$1

#1) Validar args
if [ $# -eq 0 ]
then
  echo "Error: num args incorrecte"
  echo "Usage: $0 mes"
  exit $ERR_NARGS
fi

# mesos
# dies
for mes in $*
do
	if ! [ $mes -ge 1 -a $mes -le 12 ]
	then
 		 echo "Error, mes $mes no vàlid, han de ser valors entre el 1 i el 12" >> /dev/stderr
	 else
		case $mes in
  		"2")
	       	dies=28;;
  		"4"|"6"|"9"|"11")
     		dies=30;;
  		*)
     		dies=31;;
		esac
		echo "El mes $mes, té $dies dies"
fi
done
exit 0

