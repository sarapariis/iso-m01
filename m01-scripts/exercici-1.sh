# !/bin/bash
# @Sara París
# Febrer 2024
# 
# Mostrar l’entrada estàndard numerant línia a línia
# ---------------------------------------------
num=1
while read -r line
do
   echo "$num: $line"
   ((num++))
done
exit 0
