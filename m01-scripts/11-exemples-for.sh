# !/bin/bash
# @Sara París
# Febrer 2024
# Descripció: exemples bucle for
# ------------------------------------
#8)llistar els login ordenats i numerats
#cut -d: -f1 /etc/passwd | sort
num=1
llistat=$(cut -d: -f1 /etc/passwd | sort)
for elem in $llistat
do
   echo "$num: $elem"
   ((num++))
done
exit 0
#7) llistar numerant les linies
num=1
llistat=$(ls)
for elem in $llistat
do
   echo "$num: $elem"
   ((num++))
done
exit 0

#6) iterar pel resultat d'executar la ordre ls
llistat=$(ls)
for elem in $llistat
do
   echo "$elem"
done

exit 0

#5) numerar arguments
num=1
for arg in $*
do
   echo "$num: $arg"
   num=$((num +1))
done 
exit 0

#4) $@ expandeix $* no
for arg in "$@"
do 
   echo "$arg"
done
exit 0


# 3)iterar per la llista d'arguments
for arg in  "$*"
do
   echo "$arg"
done
exit 0

#2) iterar noms
for nom in "pere" "marta" "anna" "pau"

#
# 1) iterar noms
for nom in "pere" "marta" "anna" "pau"
do
  echo "$nom"
done
exit 0
