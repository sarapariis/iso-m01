# !/bin/bash
# @Sara París
# Febrer 2024
# prog dir...
# a) rep un arg i es un directori es llista
# b) llistar numerant els elements del directori
# c) epr cada element dir si es dir, regular o altra cosa
# ---------------------------------------------
ERR_NARGS=1
ERR_NODIR=2
# 1) validar arguments
if [ $# -eq 0 ]
then
  echo "Error: número args no vàlid"
  echo "usage: $0 dir..."
  exit $ERR_NARGS
fi

for dir in $*
do
  if ! [ -d $dir ]
  then
    echo "Error: $dir no és un directori" 1>&2
  else
    llista_dir=$(ls $dir)
    echo "dir: $dir"
    for nom in $llista_dir
    do
      if [ -h "$dir/$nom" ]; then
        echo -e "\t$nom és un link"
      elif [ -d "$dir/$nom"  ]; then
        echo -e "\t$nom és un dir"
      elif [ -f "$dir/$nom" ]; then
        echo -e "\t$nom és un regular"      	  
      else
       echo -e "\t$nom és una altra cosa"
      fi  
    done	
  fi
done  
exit 0

