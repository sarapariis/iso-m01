# !/bin/bash
# @Sara París
# Febrer 2024
# prog dir
# a) rep un arg i es un directori es llista
# b) llistar numerant els elements del directori
# c) epr cada element dir si es dir, regular o altra cosa
# ---------------------------------------------
ERR_ARGS=1
ERR_NODIR=2
# si num args no es correcte plegar
if [ $# -ne 1 ]; then
  echo "ERROR: num args incorrecte"
  echo "usage: $0 dir"
  exit $ERR_ARGS
fi
dir=$1
# si no és un directori plegar
if [ ! -d $dir  ]; then
  echo "ERROR: $dir no és un directori"
  echo "usage: $0 dir"
  exit $ERR_NODIR
fi
# xixa: llistar el directori
llistat=$(ls $dir)
for elem in $llistat
do
	if [ -f $dir/$elem ]
	then
		echo "$elem is a regular file"
	elif [ -d $dir/$elem ]
	then
		echo "$elem is a directory"
	else
		echo "$elem is another thing"
	fi	
done
exit 0

