# !/bin/bash
# @Sara París
# Febrer 2024
# Fer un comptador des de zero fins al valor indicat per l’argument rebut.
# -------------------------------------------------------------
ERR_NARGS=2
# 1) validar arg.
if [ $# -ne 1 ]
then
  echo "Error numero arguments incorrecte"
  echo "Usage: $0 nota"
  exit $ERR_NARGS
fi	
# 2) contador
MAX=$1
num=0
while [ $num -le $MAX ]
do
   echo "$num: $arg"
   ((num++))
done
exit 0

