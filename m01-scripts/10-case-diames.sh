# !/bin/bash
# @Sara París
# Febrer 24
# Descripció --> dir els dies que te el mes
# Synopsis: prog arg
# 	a) validar arguments
# 	b) validar mes [1-12]
# 	c) xixa
# ----------------------------------------
#
ERR_NARGS=1
ERR_ARGVL=2
if [ $# -ne 1 ]
then
  echo "Error: Més arguments dels demanats"
  echo "Usage: $0 mes"
  exit $ERR_NARGS
fi
mes=$1
if ! [ $mes -ge 1 -a $mes -le 12 ]
then
  echo "Error, mes $mes no vàlid"
  echo "Mes pren valors de [1-12]"
  echo "Usage: $0 mes"
  exit $ERR_ARGVL
fi 

case $mes in
  "2")
     dies=28;;
  "4"|"6"|"9"|"11")
     dies=30;;
  *)
     dies=31;;

esac
echo "El mes $mes, té $dies dies"
exit 0
