# !/bin/bash
# @Sara París
# Febrer 2024
# Mostrar línia a línia l’entrada estàndard, retallant només els primers 50 caràcters.
# ----------------------------------------------------
# 1) fer el cut
while read -r line
do
	echo "$line" | cut -c 1-50
done
exit 0

